# Bot Telegram #

## VERSÕES

### Python 3
### Php 7
### Laravel 6

## BANCO DE DADOS

### Mysql 5.7

## DOCKER

## UBUNTU 20.04

## BIBLIOTECA PARA TESTES

### PhpUnit

## PASSOS PARA A EXECUÇÃO DO PROJETO

* git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/desafio.git

* Arquivo contendo os links dos bots referentes ao ambiente de **desenvolvimento** e de
**produção**, token do bot referente ao ambiente de desenvolvimento, dados usados
na configuração do banco de dados para serem utilizados no **bot-python.dockerfile** e
**docker-compose.yml**, tudo isso foi disponibilizado no arquivo Token-BD.txt como um guia

* **No ambiente de produção só foi subido a aplicação python do bot, a api em Laravel não foi subida, mais detalhes na seção nomeada de "AMBIENTE DE PRODUÇÃO"**

* Após atribuição dos dados, estando dentro do diretório desafio, execute os comandos abaixo:

* docker-compose build

* docker-compose up -d

* Para saber se todos os containers inicializaram: docker ps -a

* **FOI ADICIONADO UM WAIT PARA SÓ EFETUAR OS COMANDOS DEFINIDOS NO SERVIÇO APILARAVEL, QUANDO O BANCO DE DADOS REFERENTE AO SERVIÇO BD-SERVER ESTIVER DISPONÍVEL**

* **PARA SABER SE O BD-SERVER JÁ ESTÁ DISPONÍVEL EXECUTE O COMANDO:**

* **docker logs -t api-bot**

* **Se apareceram as mensagens abaixo, quer dizer que ocorreu tudo certo**

* **Database seeding completed successfully. Laravel development server started: http://0.0.0.0:8000**

* Com isso já é possível consumir a api através de um http client ou usar o bot referente ao ambiente de desenvolvimento

* Acesse o bot referente ao ambiente de desenvolvimento, clique em **Começar** e digite
**/start** para iniciar a conversa

* No bot de desenvolvimento irá aparecer uma mensagem com botões em que o usuário poderá
seguir o fluxo desejado

* **Importante: Ao finalizar um atendimento é enviado um aúdio, sendo que este arquivo é armazenado em um diretório nomeado de "audio" que criei através da biblioteca "os" no arquivo audio.py e ficará localizado dentro do diretório "bot-telegram-python", pois sem ele o aúdio não é enviado e não é possível finalizar a conversa, finalizar o ConversationHandler.**

## AMBIENTE DE PRODUÇÃO

* Para o ambiente de produção foi utilizado o **Heroku** e foi subido somente a 
aplicação em python do bot, portanto a api em laravel não foi subida

* Assim a conversa com o bot referente ao ambiente de produção não será do mesmo
nível da conversa com o bot referente ao ambiente de desenvolvimento

## COMO INICIAR A CONVERSA COM O BOT EM PRODUÇÃO

* **O email da pessoa vinculado a conta do Heroku deve ter sido adicionado por mim na minha aplicação do bot em python que roda no Heroku, para executar os passos abaixo com sucesso**
* **heroku login**
* **Efetue o login**
* **heroku restart -a desafio-bot-telegram (este passo não é obrigatório, inseri ele caso ao executar o comando abaixo de logs, mostrar a aplicação crashada)**
* **heroku logs -t -a desafio-bot-telegram**
* **Assim será possível acompanhar os logs do bot em python no Heroku**
* **Digite no bot de produção: /start**
* **Sendo retornado uma mensagem informando: Olá "seu nome e sobrenome", infelizmente no momento a api não está em produção 🙃**
* **Lembrando que essa aplicação no Heroku só consegue comunicar-se com o bot referente ao ambiente de produção**

## UPLOAD DE ARQUIVOS JSON E CSV

* Acesse o diretório desafio

* cd api-bot-telegram

* cd data

* Dentro deste diretório data, existirão 2 arquivos (chats.csv e chats.json) que poderão ser utilizados para fazer upload através da rota: /api/chats-import

## EXECUTAR OS TESTES

* Acesse o diretório desafio

* cd api-bot-telegram

* composer install

* crie um arquivo .env baseado em .env.example

* php artisan key:generate

* ./vendor/bin/phpunit

### Insomnia-Api-Bot.json armazena a coleção da api no insomnia

### ALGUMAS DAS BIBLIOTECAS USADAS

* python-decouple
* python-telegram-bot
* python-telegram-bot-pagination
* gTTS
* requests

## MELHORIAS FUTURAS

* Hoje os nomes das categorias (Telefone e Internet) são colocadas de forma estática no código do bot em python, seria interessante trazer os nomes das categorias através da api

* Subir a api em laravel para produção (Heroku) e assim aumentar o nível da conversa
com o bot referente ao ambiente de produção
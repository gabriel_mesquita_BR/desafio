<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Answer extends Model
{
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categoria_id');
    }

    public static function searchByCategory($category_id)
    {
        return static::where('categoria_id', $category_id)->get();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];

    public function answers()
    {
        return $this->hasMany(Answer::class, 'categoria_id');
    }
}

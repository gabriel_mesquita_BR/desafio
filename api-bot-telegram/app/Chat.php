<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Chat extends Model
{
    protected $guarded = ['id'];

    public static function jsonToArray($chatFile, $nameFile)
    {
        $chatFile->storeAs('chats', $nameFile);

        $pathRelative = 'chats/' . $nameFile;

        $pathAbsolute = Storage::path($pathRelative);

        $file = file_get_contents($pathAbsolute);

        $file = json_decode($file, true);

        return $file;
    }
}

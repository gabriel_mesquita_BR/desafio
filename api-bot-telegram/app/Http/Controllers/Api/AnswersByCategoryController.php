<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnswerIndexResource;
use Illuminate\Http\Request;

class AnswersByCategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $category_id)
    {
        $answers = Answer::searchByCategory($category_id);

        return AnswerIndexResource::collection($answers);
    }
}

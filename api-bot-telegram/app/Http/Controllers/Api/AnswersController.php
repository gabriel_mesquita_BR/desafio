<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnswerRequest;
use App\Http\Resources\AnswerIndexResource;
use App\Http\Resources\AnswerShowResource;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AnswerIndexResource::collection(Answer::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnswerRequest $request)
    {
        Answer::create($request->all());
        return response()->json(['message' => 'Resposta criada com sucesso'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $answer = Answer::findOrFail($id);
            return new AnswerShowResource($answer);

        }catch(\Exception $exception) {
            abort(404, "Resposta não encontrada");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnswerRequest $request, $id)
    {
        try {

            $answer = Answer::findOrFail($id);
            $answer->update($request->all());

            return response()->json(
                ['message' => 'Resposta atualizada com sucesso'], 200
            );

        }catch(\Exception $exception) {
            abort(404, "Resposta não encontrada");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $answer = Answer::findOrFail($id);
            $answer->delete();

            return response()->json(null, 204);

        }catch(\Exception $exception) {
            abort(404, "Resposta não encontrada");
        }
    }
}

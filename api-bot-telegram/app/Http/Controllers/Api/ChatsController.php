<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChatRequest;
use App\Http\Resources\ChatIndexResource;
use Illuminate\Http\Request;

class ChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ChatIndexResource::collection(Chat::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChatRequest $request)
    {
        Chat::create($request->all());
        return response()->json(['message' => 'Mensagem recebida'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $name, lastname
     * @return \Illuminate\Http\Response
     */
    public function show($name, $lastname)
    {
        $chats = Chat::where('nome', $name)->where('sobrenome', $lastname)->get();

        return ChatIndexResource::collection($chats);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $chat_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($chat_id)
    {
        $chat = Chat::where('chat_id', $chat_id)->first();

        if($chat !== null) {
            $chat->delete();
            return response()->json(null, 204);
        }

        abort(404, "Conversa não encontrada");
    }
}

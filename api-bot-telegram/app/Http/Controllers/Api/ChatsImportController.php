<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Http\Controllers\Controller;
use App\Imports\ChatsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ChatsImportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {

            $chatFile      = $request->file('conversa');
            $nameFile      = $chatFile->getClientOriginalName();
            $extensionFile = $chatFile->getClientOriginalExtension();
            $file          = null;

            if(strcasecmp($extensionFile, 'json') == 0) {

                $file = Chat::jsonToArray($chatFile, $nameFile);

                // check if it's matrix
                if(array_key_exists('0', $file)) {
                    foreach($file as $data)
                        Chat::create($data);
                }else {
                    Chat::create($file);
                }

            }else if(strcasecmp($extensionFile, 'csv') == 0) {

                Excel::import(new ChatsImport, $chatFile->storeAs('chats', $nameFile));
            }

            return response()->json(
                ['message' => 'Conversas importadas com sucesso'], 200
            );

        }catch(\Error $error) {
            return response()->json(
                ['message' => 'Falha ao importar as conversas'], 500
            );
        }
    }
}

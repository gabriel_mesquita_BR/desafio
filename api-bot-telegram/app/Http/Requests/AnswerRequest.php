<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'solucao'      => 'required|min:10',
            'categoria_id' => 'required|exists:categories,id'
        ];
    }

    public function messages()
    {
        return [
            'solucao.required'      => 'Solução é obrigatória',
            'solucao.min'           => 'Solução deve ter no mínimo 10 caracteres',
            'categoria_id.required' => 'Categoria é obrigatória',
            'categoria_id.exists'   => 'Categoria não existe',
        ];
    }
}

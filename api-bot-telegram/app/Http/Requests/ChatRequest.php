<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'sobrenome' => 'required',
            'mensagem'  => 'required',
            'chat_id'   => 'required|unique:chats'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'      => 'Nome é obrigatório',
            'sobrenome.required' => 'Sobrenome é obrigatório',
            'mensagem.required'  => 'Mensagem é obrigatório',
            'chat_id.required'   => 'Id do chat é obrigatório',
            'chat_id.unique'     => 'Id do chat já existe',
        ];
    }
}

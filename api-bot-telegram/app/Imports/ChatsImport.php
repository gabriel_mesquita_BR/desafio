<?php

namespace App\Imports;

use App\Chat;
use Maatwebsite\Excel\Concerns\ToModel;

class ChatsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Chat([
            'nome'      => $row[0],
            'sobrenome' => $row[1],
            'mensagem'  => $row[2],
            'chat_id'   => $row[3],
        ]);
    }
}

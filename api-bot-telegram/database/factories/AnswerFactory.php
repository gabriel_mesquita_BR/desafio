<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    return [
        'solucao'      => $faker->text,
        "categoria_id" => $faker->numberBetween(1, 2)
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Chat;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
    return [
        'nome'      => $faker->firstName,
        'sobrenome' => $faker->lastName,
        'mensagem'  => $faker->text(20),
        'chat_id'   => $faker->unique()->randomNumber($nbDigits = 4, $strict = true)
    ];
});

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('chats', 'Api\ChatsController')->only(['index', 'store', 'destroy']);

Route::get('chats/{name}/{lastname}', 'Api\ChatsController@show')->name('chats.show');

Route::apiResource('categories', 'Api\CategoriesController')->except(['create', 'edit']);

Route::apiResource('answers', 'Api\AnswersController')->except(['create', 'edit']);

Route::get('answers-by-category/{category_id}', 'Api\AnswersByCategoryController')
    ->name('answers-by-category.show');

Route::post('chats-import', 'Api\ChatsImportController')->name('chats-import.store');

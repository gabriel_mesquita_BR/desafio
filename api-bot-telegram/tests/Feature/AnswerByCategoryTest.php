<?php

namespace Tests\Feature;

use App\Answer;
use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AnswerByCategoryTest extends TestCase
{

    use RefreshDatabase;

    public function setUp():void
    {
        parent::setUp();

        $categories = factory(Category::class, 2)->create();

        $answersOne = factory(Answer::class, 4)->make();

        $categories[0]->answers()->saveMany($answersOne);

        $answersTwo = factory(Answer::class, 3)->make();

        $categories[1]->answers()->saveMany($answersTwo);
    }

    public function testShouldListAnswersByCategory()
    {
        $this->get(route('answers-by-category.show', 1))
             ->assertStatus(200)
             ->assertJsonStructure(['data' => [
                '*' => ['id', 'solucao', 'categoria' => ['id', 'nome']]]]);
    }
}

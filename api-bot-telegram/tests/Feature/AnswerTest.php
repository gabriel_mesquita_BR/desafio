<?php

namespace Tests\Feature;

use App\Answer;
use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AnswerTest extends TestCase
{

    use RefreshDatabase;

    public function testShouldListAnswers()
    {
        $categories = factory(Category::class, 2)->create();

        $answersOne = factory(Answer::class, 3)->make();

        $categories[0]->answers()->saveMany($answersOne);

        $answersTwo = factory(Answer::class, 2)->make();

        $categories[1]->answers()->saveMany($answersTwo);

        $this->get(route('answers.index'))->assertStatus(200)
             ->assertJsonStructure(['data' => [
                '*' => ['id', 'solucao', 'categoria' => ['id', 'nome']]]])
             ->assertJsonCount($answersOne->count() + $answersTwo->count(), 'data');
    }

    public function testShouldStoreAnswer()
    {
        $category = factory(Category::class)->create();

        $data = [
            'solucao'      => 'Cheque se o telefone está no gancho.',
            'categoria_id' => $category->id
        ];

        $this->post(route('answers.store'), $data)
             ->assertStatus(201)
             ->assertJson(['message' => 'Resposta criada com sucesso']);
    }

    public function testShouldShowAnswer()
    {
        $category = factory(Category::class)->create();

        $answer = factory(Answer::class)->make();

        $category->answers()->save($answer);

        $this->get(route('answers.show', $answer->id))
             ->assertStatus(200)
             ->assertJsonStructure(['data' => [
                'id', 'solucao', 'categoria' => ['id', 'nome']]]);
    }

    public function testShouldUpdateAnswer()
    {
        $category = factory(Category::class)->create();

        $answer = factory(Answer::class)->make();

        $category->answers()->save($answer);

        $data = [
            'solucao'      => 'Cheque se todos os telefones estão no gancho',
            'categoria_id' => $category->id
        ];

        $this->put(route('answers.update', $answer->id), $data)
             ->assertStatus(200)
             ->assertJson(['message' => 'Resposta atualizada com sucesso']);
    }

    public function testShouldDestroyAnswer()
    {
        $category = factory(Category::class)->create();

        $answer = factory(Answer::class)->make();

        $category->answers()->save($answer);

        $this->delete(route('answers.destroy', $answer->id))
             ->assertStatus(204);
    }
}

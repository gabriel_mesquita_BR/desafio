<?php

namespace Tests\Feature;

use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{

    use RefreshDatabase;

    public function testShouldListCategories()
    {
        $categories = factory(Category::class, 2)->create();

        $this->get(route('categories.index'))->assertStatus(200)
             ->assertJsonStructure(['data' => ['*' => ['id', 'nome']]])
             ->assertJsonCount($categories->count(), 'data');
    }

    public function testShouldStoreCategory()
    {
        $data = [
            'nome' => 'Internet'
        ];

        $this->post(route('categories.store'), $data)
             ->assertStatus(201)
             ->assertJson(['message' => 'Categoria criada com sucesso']);
    }

    public function testShouldShowCategory()
    {
        $category = factory(Category::class)->create();

        $this->get(route('categories.show', $category->id))
             ->assertStatus(200)
             ->assertJsonStructure(['data' => ['id', 'nome']]);
    }

    public function testShouldUpdateCategory()
    {
        $category = factory(Category::class)->create();

        $data = [
            'nome' => 'Net'
        ];

        $this->put(route('categories.update', $category->id), $data)
             ->assertStatus(200)
             ->assertJson(['message' => 'Categoria atualizada com sucesso']);
    }

    public function testShouldDestroyCategory()
    {
        $category = factory(Category::class)->create();

        $this->delete(route('categories.destroy', $category->id))
             ->assertStatus(204);
    }
}

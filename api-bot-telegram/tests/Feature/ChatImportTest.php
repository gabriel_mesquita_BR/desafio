<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Http\UploadedFile;

class ChatImportTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldImportChatCsv()
    {
        $row1 = 'Natanael, Alves, /start, 9821';
        $row2 = 'Matheus, Silva, /start, 2931';

        $content = implode("\n", [$row1, $row2]);

        $data = [
            'conversa' => UploadedFile::fake()->createWithContent(
                'arquivo.csv', $content)
        ];

        $response = $this->post(route('chats-import.store'), $data);

        $response->assertStatus(200)
                 ->assertJson(['message' => 'Conversas importadas com sucesso']);
    }

    public function testShouldImportChatJson()
    {
        $array = [
            'nome'      => "Gabriel",
            'sobrenome' => "Nunes",
            'mensagem'  => "/start",
            'chat_id'   => 1234
        ];

        $content = json_encode($array);

        $data = [
            'conversa' => UploadedFile::fake()->createWithContent(
                'arquivo.json', $content)
        ];

        $response = $this->post(route('chats-import.store'), $data);

        $response->assertStatus(200)
                 ->assertJson(['message' => 'Conversas importadas com sucesso']);
    }
}

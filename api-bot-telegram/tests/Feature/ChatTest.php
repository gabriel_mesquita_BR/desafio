<?php

namespace Tests\Feature;

use App\Chat;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChatTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldListChats()
    {
        $chats = factory(Chat::class, 3)->create();

        $this->get(route('chats.index'))
             ->assertStatus(200)
             ->assertJsonStructure([
                'data' => ['*' => ['id', 'nome', 'sobrenome', 'mensagem', 'chat_id']]
            ])
            ->assertJsonCount($chats->count(), 'data');
    }

    public function testShouldStoreChat()
    {
        $data = [
            'nome'      => 'Gabriel',
            'sobrenome' => 'Nunes',
            'mensagem'  => '/start',
            'chat_id'   => 1252
        ];

        $this->post(route('chats.store'), $data)
             ->assertStatus(201)
             ->assertJson(['message' => 'Mensagem recebida']);
    }

    public function testShouldShowChat()
    {
        $chat = factory(Chat::class)->create();

        $this->get(route('chats.show', [
            'name' => $chat->nome, 'lastname' => $chat->sobrenome
        ]))->assertStatus(200)
           ->assertJsonStructure([
            'data' => ['*' => ['id', 'nome', 'sobrenome', 'mensagem', 'chat_id']]
        ])
           ->assertJsonCount($chat->count(), 'data');
    }

    public function testShouldDestroyChat()
    {
        $chat = factory(Chat::class)->create();

        $this->delete(route('chats.destroy', $chat->chat_id))->assertStatus(204);
    }
}

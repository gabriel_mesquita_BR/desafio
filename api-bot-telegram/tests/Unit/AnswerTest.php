<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;

use App\Answer;
use Tests\TestCase;

class AnswerTest extends TestCase
{
    public function testAnswerColumnMassAssignment()
    {
        $answer = new Answer();

        // don't expect id in the mass assignment
        $notExpected = ['id'];

        $arrayCompared = array_diff($notExpected, $answer->getGuarded());

        $this->assertEquals(0, count($arrayCompared));
    }

    public function testStoreShouldThrowAnErrorIfSolutionIsMissing()
    {
        $this->json('POST', route('answers.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['solucao']]);
    }

    public function testStoreShouldThrowAnErrorIfCategoryIdIsMissing()
    {
        $this->json('POST', route('answers.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['categoria_id']]);
    }

    public function testStoreShouldThrowAnErrorIfSolutionIsLessThanTheExpected()
    {
        $this->json('POST', route('answers.store'), ['solucao' => 'checar'])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['solucao']]);
    }
}

<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;

use App\Category;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function testCategoryColumnMassAssignment()
    {
        $category = new Category();

        // don't expect id in the mass assignment
        $notExpected = ['id'];

        $arrayCompared = array_diff($notExpected, $category->getGuarded());

        $this->assertEquals(0, count($arrayCompared));
    }

    public function testStoreShouldThrowAnErrorIfNameIsMissing()
    {
        $this->json('POST', route('categories.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['nome']]);
    }
}

<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;

use App\Chat;
use Tests\TestCase;

class ChatTest extends TestCase
{
    public function testChatColumnMassAssignment()
    {
        $chat = new Chat();

        // don't expect id in the mass assignment
        $notExpected = ['id'];

        $arrayCompared = array_diff($notExpected, $chat->getGuarded());

        $this->assertEquals(0, count($arrayCompared));
    }

    public function testStoreShouldThrowAnErrorIfNameIsMissing()
    {
        $this->json('POST', route('chats.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['nome']]);
    }

    public function testStoreShouldThrowAnErrorIfLastNameIsMissing()
    {
        $this->json('POST', route('chats.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['sobrenome']]);
    }

    public function testStoreShouldThrowAnErrorIfMessageIsMissing()
    {
        $this->json('POST', route('chats.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['mensagem']]);
    }

    public function testStoreShouldThrowAnErrorIfChatIdIsMissing()
    {
        $this->json('POST', route('chats.store'), [])
             ->assertStatus(422)
             ->assertJsonStructure(['errors' => ['chat_id']]);
    }
}

import requests

class Request:

    @staticmethod
    def startChat(name, lastname, message, chat_id):

        data = {'nome': name, 'sobrenome': lastname, 'mensagem': message, 
            'chat_id': chat_id}
        
        return requests.post(
            'http://apilaravel:8000/api/chats', data=data
        )

    @staticmethod
    def deleteChat(chat_id):
        
        return requests.delete(
            'http://apilaravel:8000/api/chats/' + str(chat_id)
        )

    @staticmethod
    def getCategory(category_id):
        return requests.get(
            'http://apilaravel:8000/api/answers-by-category/' + str(category_id)
        ).json()

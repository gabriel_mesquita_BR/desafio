from gtts import gTTS
import os

class Audio():
    
    @staticmethod
    def convert_text_to_speech(msg):
        audio = gTTS(text=msg, lang='pt', slow=False, tld='com.br')

        if(not os.path.exists('./audio')):
            os.mkdir('./audio')

        audio.save("./audio/goodbye.mp3")

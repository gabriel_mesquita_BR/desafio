import logging
import sys
import telegram

from decouple import config
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.bot import Bot
from telegram.ext import (CallbackQueryHandler, CommandHandler,
                          ConversationHandler, Updater)
from telegram_bot_pagination import InlineKeyboardPaginator

from api.request import Request
from audio import Audio
from constants import (INTERNET_ID, MODE, PHONE_ID, RESPONSE_HELP,
                       RESPONSE_PAGINATOR, RESPONSE_START, TOKEN)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)

logger = logging.getLogger()

if MODE == 'dev':
    def run(updater):
        updater.start_polling()
        updater.idle()

elif MODE == 'prod':
    def run(updater):
        PORT = config('PORT', default='8443', cast=int)
        HEROKU_APP_NAME = config('HEROKU_APP_NAME')

        updater.start_webhook(listen='0.0.0.0', port=PORT, url_path=TOKEN,
            webhook_url=f'https://{HEROKU_APP_NAME}.herokuapp.com/{TOKEN}')

else:
    logger.info('Modo de ambiente não especificado')
    sys.exit()

def start(update, context):
    name = update.effective_user['first_name']
    lastname = update.effective_user['last_name']
    message = update.message.text
    chat_id = update.message.chat.id

    logger.info(f"O usuário {name} {lastname}, iniciou a sessão (conversa)")

    if MODE == 'dev':
        response = Request.startChat(name, lastname, message, chat_id)

        message = response.json()['message']

    elif MODE == 'prod':
        message = (f"Olá {name} {lastname}, infelizmente no momento a api não "
                    "está em produção 🙃")
        update.message.reply_text(message)
    
        return ConversationHandler.END
    
    keyboard = [
        [
            InlineKeyboardButton("Ajuda", callback_data='ajuda'),
            InlineKeyboardButton("Finalizar Atendimento", 
                callback_data='finalizar_atendimento'),
        ]
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text(
        f'{message}.\nOlá {name} {lastname}, seja bem-vindo 😄.\n'
        "Por favor, escolha uma das opções abaixo para prosseguir.", 
        reply_markup=reply_markup)
    
    return RESPONSE_START

def response_start(update, context):
    query = update.callback_query

    bot = context.bot

    if query.data == 'ajuda':
        logger.info(f"O usuário {update.effective_user['first_name']} " +
            f"{update.effective_user['last_name']}, solicitou ajuda")

        keyboard = [
            [
                InlineKeyboardButton("Telefone Fixo", 
                    callback_data='telefone_fixo'),
                InlineKeyboardButton("Internet", callback_data='internet'),
            ]
        ]
        
        reply_markup = InlineKeyboardMarkup(keyboard)

        bot.sendMessage(
            chat_id=query.message.chat_id,
            text="Por favor, qual categoria abaixo está relacionada ao seu problema?",
            reply_markup=reply_markup
        )

        return RESPONSE_HELP
    
    elif query.data == 'finalizar_atendimento':
        query = update.callback_query
        user = update.effective_user['first_name'] + update.effective_user['last_name']
        
        bot_media = Bot(token=telegram.Bot(token=TOKEN).token)
        
        logger.info(f"O usuário {user}, finalizou a sessão (conversa)")

        Request.deleteChat(query.message.chat.id)

        message_text = (f'{user}, você escolheu finalizar o atendimento, até mais.')

        Audio.convert_text_to_speech(message_text)

        bot_media.send_audio(
            chat_id=update.effective_user['id'], 
            audio=open('./audio/goodbye.mp3', 'rb'),
            title='atendimento_finalizado')

        return ConversationHandler.END

def response_help(update, context):    
    query = update.callback_query
    selected_option = ''
    results = None
    data = []
    
    user = update.effective_user['first_name'] + update.effective_user['last_name']

    if query.data == 'telefone_fixo':
        logger.info(f"O usuário {user}, selecionou a categoria de Telefone")
        selected_option = 'Telefone Fixo'
        results = Request.getCategory(PHONE_ID)

    elif query.data == 'internet':
        logger.info(f"O usuário {user}, selecionou a categoria de Internet")
        selected_option = 'Internet'
        results = Request.getCategory(INTERNET_ID)

    for result in results['data']:
        data.append(result['solucao'])

    response_help.var = data

    paginator = InlineKeyboardPaginator(
        len(data),
        data_pattern='character#{page}'
    )

    paginator.add_after(InlineKeyboardButton('Ajuda', callback_data='ajuda'))

    paginator.add_after(InlineKeyboardButton('Finalizar Atendimento', 
        callback_data='finalizar_atendimento'))

    context.bot.sendMessage(
        chat_id=query.message.chat_id,
        text=f"Abaixo, listamos soluções possíveis para *{selected_option}*",
        parse_mode="MarkdownV2"
    )

    context.bot.sendMessage(
        chat_id=query.message.chat_id,
        text=data[0],
        reply_markup=paginator.markup,
        parse_mode='Markdown'
    )

    return RESPONSE_PAGINATOR

def characters_page_callback(update, context):
    query = update.callback_query

    solutions = response_help.var

    page = int(query.data.split('#')[1])

    paginator = InlineKeyboardPaginator(
        len(solutions),
        current_page=page,
        data_pattern='character#{page}'
    )

    paginator.add_after(InlineKeyboardButton('Ajuda', callback_data='ajuda'))

    paginator.add_after(InlineKeyboardButton('Finalizar Atendimento', 
        callback_data='finalizar_atendimento'))

    query.edit_message_text(
        text=solutions[page - 1],
        reply_markup=paginator.markup,
        parse_mode='Markdown'
    )

if __name__ == "__main__":
    my_bot = telegram.Bot(token=TOKEN)

updater = Updater(my_bot.token, use_context=True)

dispatcher = updater.dispatcher

conv_handler = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        RESPONSE_START : [CallbackQueryHandler(response_start)],
        RESPONSE_HELP : [CallbackQueryHandler(response_help)],
        RESPONSE_PAGINATOR : [
            CallbackQueryHandler(characters_page_callback, pattern='^character#'),
            CallbackQueryHandler(response_start)
        ]
    },
    fallbacks=[],
)

dispatcher.add_handler(conv_handler)

run(updater)

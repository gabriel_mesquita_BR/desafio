from decouple import config

RESPONSE_START, RESPONSE_HELP, RESPONSE_PAGINATOR = range(3)

PHONE_ID, INTERNET_ID = range(1, 3)

TOKEN = config('TOKEN_TELEGRAM')
MODE = config('MODE', default='dev')

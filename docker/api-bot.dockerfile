FROM php:7.2

LABEL maintainer="Gabriel"

ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

RUN apt-get update -y && apt-get install -y zip unzip git libpng-dev libzip-dev

RUN docker-php-ext-install pdo pdo_mysql gd zip

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ADD /api-bot-telegram /api-bot

WORKDIR /api-bot

RUN composer install

ADD /api-bot-telegram/.env.example /api-bot/.env
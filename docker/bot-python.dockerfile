FROM python:3.8-alpine

LABEL maintainer="Gabriel"

ENV TOKEN_TELEGRAM=

RUN pip install --upgrade pip \
    && mkdir /app

ADD /bot-telegram-python /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD python bot.py